package dto;

import lombok.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@JsonSerialize
public class UserOut {

    private long code;
    private String message;
    private String type;

}

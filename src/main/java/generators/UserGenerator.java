package generators;

import dto.User;

public class UserGenerator {
    private  static long COUNT = 0;
    public static User getNewUser(){
        COUNT++;
        return User.builder()
                .id(COUNT)
                .email("Email")
                .firstName("First Name")
                .lastName("Last Name")
                .password("Password" + COUNT)
                .phone("8(999)999-99-99")
                .username("Username" + COUNT)
                .userStatus(21L)
                .build();
    }
    public static User getExistUser(){
        return User.builder()
                .id(1L)
                .email("Email")
                .firstName("First Name")
                .lastName("Last Name")
                .password("Password" + COUNT)
                .phone("8(999)999-99-99")
                .username("Username" + COUNT)
                .userStatus(21L)
                .build();
    }
}

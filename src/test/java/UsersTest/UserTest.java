package UsersTest;

import dto.User;
import dto.UserOut;
import generators.UserGenerator;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import services.UserService;

public class UserTest {
    @Test
    public void createUserTest(){
        //создание пользователя
        User user = UserGenerator.getNewUser();
//        User user = User.builder()
//                .id(1L)
//                .email("Email")
//                .firstName("First Name")
//                .lastName("Last Name")
//                .password("Password")
//                .phone("8(999)999-99-99")
//                .username("Username")
//                .userStatus(21L)
//                .build();

        //запрос на сервер
        UserService userService = new UserService();
        Response response = userService.CreateUser(user);

        UserOut actual = response.then()
                .log().all()
                .assertThat()
                .statusCode(200)
                .and()
                .extract()
                .body()
                .as(UserOut.class);

        UserOut expected = UserOut.builder()
                .code(200L)
                .message(user.getId().toString())
                .type("unknown")
                .build();

        Assertions.assertEquals(expected,actual);
    }

    @ParameterizedTest
    @ValueSource(strings = {"admin","moderator","user"})
    public void getUserByName(String name){
        UserService userService = new UserService();
        Response response = userService.GetUserByName(name);
        UserOut actual = response.then()
                .log().all()
                .assertThat()
                .statusCode(404)
                .and()
                .extract()
                .body()
                .as(UserOut.class);

        UserOut expected = UserOut.builder()
                .code(1L)
                .message("User not found")
                .type("error")
                .build();

        Assertions.assertEquals(expected,actual);
    }

}
